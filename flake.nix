{
  description = "Oro's Nix flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    nixos-hardware.url = "github:nixos/nixos-hardware";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    nixpkgs,
    home-manager,
    ...
  }: let
    forAllSystems = nixpkgs.lib.genAttrs [
      "x86_64-linux"
      "aarch64-linux"
    ];

    nixpkgsFor = forAllSystems (system: import nixpkgs {inherit system;});
    forEachSystem = fn:
      forAllSystems (system:
        fn {
          inherit system;
          pkgs = nixpkgsFor.${system};
        });

    modules = {
      # modules.home.{common, dev}
      home = {
        common = [./users/oro/shell.nix ./users/oro/base.nix];
        # TODO: gpg. Should probably get a yubikey for it.
        dev = [./users/oro/git.nix];
      };

      # modules.nixos
      nixos = {
        common = [
          ./common/nix.nix
          ./common/personal.nix
        ];

        dev = [
          ./common/podman.nix
        ];

        services = [
          # placeholder
        ];

        # Only GNOME for now, *maybe* will add KDE, Budgie, Cinnamon, and some
        # other desktops if they support Wayland.
        desktops = {
          common = [
            ./common/desktop/audio.nix
            ./common/desktop/gui-apps.nix
          ];

          # modules.nixos.desktops.gnome
          gnome =
            [
              ./common/desktop/gnome.nix
            ]
            ++ modules.nixos.desktops.common;
        };

        #modules.nixos.server
        server = [
          ./common/podman.nix
        ];
      };
    };

    defaultModules = x:
      nixpkgs.lib.forEach ["base.nix" "filesystem.nix" "hardware.nix"] (
        mod: (./. + "/devices" + ("/" + x) + ("/" + mod))
      )
      ++ [./devices/common.nix]
      ++ [
        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.oro = {};
          home-manager.sharedModules = modules.home.common ++ modules.home.dev;
        }
      ];
  in {
    homeConfigurations = forEachSystem ({pkgs, ...}: {
      "oro" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules =
          modules.home.common
          ++ modules.home.dev;
      };
    });

    nixosConfigurations = {
      "getac-laptop" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules =
          defaultModules "getac-laptop"
          ++ modules.nixos.desktops.gnome
          ++ modules.nixos.common;
      };
    };

    formatter = forEachSystem ({pkgs, ...}: pkgs.alejandra);
  };
}
