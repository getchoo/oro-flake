{lib, ...}: {
  home = {
    username = lib.mkDefault "oro";
    homeDirectory = lib.mkDefault "/home/oro";
    stateVersion = lib.mkDefault "23.05";
  };

  home.file.".face".source = ./var/pfp.jpg;
}
