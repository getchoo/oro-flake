{pkgs, ...}: {
  programs = {
    fish = {
      enable = true;

      functions = {
        fish_greeting = "";
      };

      shellAliases = {
        cat = "bat";
      };

      plugins = let
        # takes in a list of strings based on plugins
        # already available in nixpkgs
        mkFishPlugins = builtins.map (plugin: {
          name = plugin;
          inherit (pkgs.fishPlugins.${plugin}) src;
        });
      in
        mkFishPlugins [
          "tide"
        ];
    };
  };

  home.packages = with pkgs; [
    bat
    pfetch
    ncdu

    gnupg
    direnv

    hostname
    ncurses

    neovim
    sshs
  ];
}
