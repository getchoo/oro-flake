{lib, ...}: {
  # Could be useful to take configs from ~/.gitconfig.

  programs.git = {
    enable = true;
    userName = "Dallas Strouse";
    userEmail = "dastrouses@gmail.com";
    signing = {
      # https://docs.github.com/en/authentication/managing-commit-signature-verification/telling-git-about-your-signing-key
      key = "2E317653460259B3";
      signByDefault = true;
    };
    extraConfig = {
      safe.directory = "*";
      init.defaultBranch = "main";
    };
  };
}
