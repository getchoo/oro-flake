{
  lib,
  pkgs,
  ...
}: {
  nixpkgs.config = {
    allowUnfree = true;
    allowUnsupportedSystem = true;
  };

  nix = {
    settings = {auto-optimise-store = true;};
    gc = {
      automatic = true;
      dates = "17:00";
      persistent = true;
      options = "--delete-older-than 3d";
    };
    package = lib.mkDefault pkgs.nixFlakes;
  };
}
