_: {
  # Supposedly conflicts with services.pipewire.
  hardware.pulseaudio.enable = false;

  # rtkit is recommended, might as well enable it.
  security.rtkit.enable = true;
  # NixOS already enables WirePlumber for us.
  services.pipewire = {
    enable = true;
    pulse.enable = true;
    jack.enable = true;
    alsa = {
      enable = true;
      support32Bit = true;
    };
  };
}
