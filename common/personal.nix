{pkgs, ...}: {
  users.users.oro = {
    description = "Dallas Strouse";
    isNormalUser = true;
    extraGroups = ["wheel" "users" "audio" "video" "networkmanager" "podman"];
    uid = 1000;
    packages = with pkgs; [
      gnupg
      neovim
      distrobox
      yt-dlp
      bubblewrap
    ];
  };

  systemd.user.tmpfiles.rules = [
    "L %t/discord-ipc-0 - - - - .flatpak/xyz/armcord/ArmCord/xdg-run/discord-ipc-0"
  ];

  security.sudo.wheelNeedsPassword = false;
}
