{lib, ...}: {
  boot = {
    tmpOnTmpfs = true;
  };

  networking = {
    networkmanager.enable = true;
    useDHCP = lib.mkForce true;
  };

  time.timeZone = "America/Chicago";

  boot.loader.grub = {
    enable = true;
    version = 2;
  };

  boot.plymouth = {
    enable = true;
    theme = "bgrt";
  };

  zramSwap.enable = true;
}
