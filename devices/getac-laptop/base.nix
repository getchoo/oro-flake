{pkgs, ...}: {
  boot = {
    kernelPackages = pkgs.linuxKernel.packages.linux_xanmod; # Should probably change this to a more laptop-focused and low-power kernel soon.
  };

  hardware = {
    bluetooth.enable = true;
    enableAllFirmware = true;
  };

  networking.hostName = "OroLaptop";

  system.stateVersion = "23.05";
}
