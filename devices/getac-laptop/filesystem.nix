_: {
  fileSystems = {
    "/" = {
      device = "rootfs";
      fsType = "tmpfs";
      options = ["mode=755" "size=1G"];
    };
    "/var" = {
      device = "/dev/lvm/nixos";
      fsType = "btrfs";
      options = ["subvol=@var" "compress-force=zstd"];
    };
    "/nix" = {
      device = "/dev/lvm/nixos";
      fsType = "btrfs";
      options = ["subvol=@nix" "compress-force=zstd"];
    };
    "/home" = {
      device = "/dev/lvm/home";
      fsType = "btrfs";
      options = ["subvol=@" "compress-force=zstd"];
    };
    "/swap" = {
      device = "/dev/lvm/swap";
      fsType = "btrfs";
      options = ["subvol=@swap" "compress-force=zstd" "noatime"];
    };
  };
  swapDevices = [
    {
      device = "/swap/swapfile";
      size = (1024 * 12) + (1024 * 2); # RAM size + 2 GB
    }
  ];
}
